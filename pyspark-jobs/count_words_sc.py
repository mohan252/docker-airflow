import pyspark
import os
sc = pyspark.SparkContext('local[*]')

txt = sc.textFile("/home/jovyan/work/pyspark-jobs/testfile.txt", 4)
python_lines = txt.filter(lambda line: 'python' in line.lower())

print(txt.count())
print(python_lines.count())

# with open('results.txt', 'w') as file_obj:
#     file_obj.write(f'Number of lines: {txt.count()}\n')
#     file_obj.write(f'Number of lines with python: {python_lines.count()}\n')