#!/bin/bash

# Start the first process
/usr/sbin/sshd -D &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start sshd: $status"
  exit $status
fi
echo "Started sshd"

# Start the second process
./spark-history-start.sh
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start spark history: $status"
  exit $status
fi
echo "Started spark-history"

# Install custom python package if requirements.txt is present
if [ -e "/requirements.txt" ]; then
    $(command -v pip) install -r /requirements.txt
fi

# Start the thrid process
jupyter notebook --allow-root &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start jupyter: $status"
  exit $status
fi
echo "Started jupyter"

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
  ps aux |grep sshd |grep -q -v grep
  PROCESS_1_STATUS=$?
  echo $PROCESS_1_STATUS
  ps aux |grep jupyter |grep -q -v grep
  PROCESS_3_STATUS=$?
  echo $PROCESS_3_STATUS
  # If the greps above find anything, they exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 -o $PROCESS_3_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit 1
  fi
done