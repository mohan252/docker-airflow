from airflow import DAG
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator
from datetime import datetime, timedelta

args = {'owner': 'airflow', 'start_date': datetime(2018, 9, 24) }

dag = DAG('count_words_sc', default_args=args, schedule_interval="@once")

t1_bash = """
echo 'count_words_sc' && 
/usr/local/spark/bin/spark-submit /home/jovyan/work/pyspark-jobs/count_words_sc.py
"""
t1 = SSHOperator(
    ssh_conn_id='ssh_default',
    task_id='test_ssh_operator',
    command=t1_bash,
    dag=dag)